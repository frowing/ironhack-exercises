//
//  IHImageViewCell.h
//  FirstCollectionVIew
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IHImageViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end

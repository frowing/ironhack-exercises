//
//  ViewController.m
//  FirstCollectionVIew
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "ViewController.h"

NSString *kCellIdentifier = @"kCellIdentifier";

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray *imageNames;
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UICollectionViewFlowLayout *regularLayout;
@property (nonatomic, strong) UICollectionViewFlowLayout *detailLayout;

@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;

@end

@implementation ViewController

//Exercise. 1. Two-column layout for the images

- (void)viewDidLoad
{
    [super viewDidLoad];
}

//Exercise 2. Switch between layouts

//Exercise 3. Delete selected cells

@end

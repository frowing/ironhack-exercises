//
//  AnimalViewController.m
//  SecondTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "AnimalViewController.h"

@implementation AnimalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

@end

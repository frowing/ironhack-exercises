//
//  NewAnimalViewController.m
//  SecondTableView
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "NewAnimalViewController.h"

NSString *kNewAnimalCellIdentifier = @"NewAnimalCellIdentifier";

@interface NewAnimalViewController ()

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *textField;

@end

@implementation NewAnimalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


@end

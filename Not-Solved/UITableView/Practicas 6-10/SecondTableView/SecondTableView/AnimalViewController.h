//
//  AnimalViewController.h
//  SecondTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimalViewController : UIViewController

@property (nonatomic, strong) NSString *animalName;

@end

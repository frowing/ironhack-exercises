//
//  ViewController.m
//  SecondTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "AnimalViewController.h"
#import "NewAnimalViewController.h"
#import "ViewController.h"

NSString * const kCellIdentifier = @"CellId";
NSString * const kSearchResultCellIdentifier = @"kSearchResultCellIdentifier";

@interface ViewController ()

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *animals;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

//Practica 6: Borrar Celda

//Practica 7: Borrar celdas desde el Detalle

//Practica 8: Mover celdas

//Practica 9: Añadir celdas

#pragma mark - NewAnimalViewControllerDelegate Methods

//Practica 10. Búsqueda

#pragma mark - UISearchDisplayDelegate Methods

@end

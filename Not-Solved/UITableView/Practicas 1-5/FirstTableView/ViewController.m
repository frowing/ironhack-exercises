//
//  ViewController.m
//  FirstTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "EvenNumberCell.h"
#import "ViewController.h"
#import "OddNumberCell.h"

NSString * const kEvenCellIdentifier = @"EvenCell";
NSString * const kOddCellIdentifier = @"OddCell";

@interface ViewController ()

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

//Practica 1: Crear y poblar una Table View

//Practica 2: Añadir secciones con header

//Practica 3: Personalizar las celdas

//Practica 4: Seleccionar la celda

//Practica 5: Configura el header




@end

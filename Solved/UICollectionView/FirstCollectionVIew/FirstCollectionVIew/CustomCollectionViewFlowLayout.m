//
//  CustomCollectionViewFlowLayout.m
//  FirstCollectionVIew
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "CustomCollectionViewFlowLayout.h"

@implementation CustomCollectionViewFlowLayout

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *elements = [super layoutAttributesForElementsInRect:rect];
    
    for (UICollectionViewLayoutAttributes *layoutAttributes in elements)
    {
        layoutAttributes.alpha = [self alphaForItemWithLayoutAttributes:layoutAttributes];
    }
    
    return elements;
}

- (CGFloat)alphaForItemWithLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    CGFloat distance = [self distanceToCollectionViewCenterFromItemWithLayoutAttributes:layoutAttributes];
    CGFloat alpha = 1 - (1/(self.collectionView.frame.size.width / distance));
    return alpha;
}

- (CGFloat)distanceToCollectionViewCenterFromItemWithLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    CGRect bounds = self.collectionView.bounds;
    CGPoint boundsCenter = CGPointMake(CGRectGetMidX(bounds),
                                       CGRectGetMidY(bounds));
    CGPoint cellCenter = layoutAttributes.center;
    CGPoint offsetFromCenter = CGPointMake(boundsCenter.x - cellCenter.x,
                                           boundsCenter.y - cellCenter.y);
    
    return offsetFromCenter.x;
}

@end

//
//  ViewController.m
//  FirstCollectionVIew
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "CustomCollectionViewFlowLayout.h"
#import "IHImageViewCell.h"
#import "ViewController.h"

NSString *kCellIdentifier = @"kCellIdentifier";

@interface ViewController ()<UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray *imageNames;
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UICollectionViewFlowLayout *regularLayout;
@property (nonatomic, strong) UICollectionViewFlowLayout *detailLayout;

@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;

@end

@implementation ViewController

//Exercise. 1. Two-column layout for the images

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.imageNames = [@[] mutableCopy];
    [self setUpRegularLayout];
    [self setUpCollectionView];
    [self setUpDetailLayout];
    [self setUpSwitchToDetailButton];
    self.imageNames = [@[@"shawshank.jpg",
                         @"schindlers_list.jpg",
                         @"pulp_fiction.jpg",
                         @"forrest_gump.jpg",
                         @"lamb_silence.jpg",
                         @"the_godfather.jpg",
                         @"lord_of_the_rings.jpg",
                         @"dark_night.jpg",
                         @"empire_strikes_back.jpg",
                         @"fight_club.jpg",
                         @"matrix.jpg",]
                       mutableCopy];
    self.selectedIndexPaths = [@[]mutableCopy];
    
    [self setUpDeleteButton];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.imageNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IHImageViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    NSString *imageName = self.imageNames[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:imageName];
    return cell;
}

- (void)setUpCollectionView
{
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:self.regularLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerClass:[IHImageViewCell class] forCellWithReuseIdentifier:kCellIdentifier];
    [self.view addSubview:self.collectionView];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    self.collectionView.allowsMultipleSelection = YES;
}

- (void)setUpRegularLayout
{
    self.regularLayout = [[UICollectionViewFlowLayout alloc] init];
    self.regularLayout.itemSize = CGSizeMake((self.view.frame.size.width / 2) - 15, self.view.frame.size.height / 2);
}

//Exercise 2. Switch between layouts

- (void)setUpDetailLayout
{
    self.detailLayout = [[CustomCollectionViewFlowLayout alloc] init];
    self.detailLayout.itemSize = self.view.frame.size;
    self.detailLayout.minimumInteritemSpacing = 0.0f;
    self.detailLayout.minimumLineSpacing = 0.0f;
    self.detailLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
}

- (void)setUpSwitchToDetailButton
{
    UIBarButtonItem *switchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(switchToDetailLayout)];
    self.navigationItem.rightBarButtonItem = switchButton;
}

- (void)setUpSwitchToRegularButton
{
    UIBarButtonItem *switchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(switchToRegularLayout)];
    self.navigationItem.rightBarButtonItem = switchButton;
}

- (void)switchToDetailLayout;
{
    [self.collectionView setCollectionViewLayout:self.detailLayout animated:YES];
    self.collectionView.pagingEnabled = YES;
    [self setUpSwitchToRegularButton];
}

- (void)switchToRegularLayout
{
    [self.collectionView setCollectionViewLayout:self.regularLayout animated:YES];
    self.collectionView.pagingEnabled = NO;
    [self setUpSwitchToDetailButton];
}

//Exercise 3. Delete selected cells

- (void)setUpDeleteButton
{
    UIBarButtonItem *switchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteSelectedCells)];
    self.navigationItem.leftBarButtonItem = switchButton;
}

- (void)deleteSelectedCells
{
    [self.collectionView performBatchUpdates:^{
        [self.collectionView deleteItemsAtIndexPaths:self.selectedIndexPaths];
        [self.selectedIndexPaths enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSIndexPath *indexPath = (NSIndexPath *)obj;
            [self.imageNames removeObjectAtIndex:indexPath.row];
        }];
    } completion:nil];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedIndexPaths addObject:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedIndexPaths removeObject:indexPath];
}

//Exercise 4: Create a custom layout

@end

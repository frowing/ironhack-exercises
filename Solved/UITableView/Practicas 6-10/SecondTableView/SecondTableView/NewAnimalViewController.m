//
//  NewAnimalViewController.m
//  SecondTableView
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "NewAnimalViewController.h"

NSString *kNewAnimalCellIdentifier = @"NewAnimalCellIdentifier";

@interface NewAnimalViewController ()<UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *textField;

@end

@implementation NewAnimalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kNewAnimalCellIdentifier];
    [self.view addSubview:self.tableView];
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, 200, 50)];
    self.textField.placeholder = @"Animal's name";
    
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNewAnimalCellIdentifier];
    self.textField.center = CGPointMake(self.textField.center.x, cell.frame.size.height / 2);
    [cell addSubview:self.textField];
    return cell;
}

- (void)doneButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate newAnimal:self.textField.text];
    }];
}


@end

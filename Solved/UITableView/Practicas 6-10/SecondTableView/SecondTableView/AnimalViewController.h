//
//  AnimalViewController.h
//  SecondTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AnimalViewControllerDelegate <NSObject>

@required
- (void)animalDeleted:(NSString *)animalName;

@end

@interface AnimalViewController : UIViewController

@property (nonatomic, strong) NSString *animalName;
@property (nonatomic, weak) id<AnimalViewControllerDelegate> delegate;

@end

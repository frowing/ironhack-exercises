//
//  ViewController.m
//  SecondTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "AnimalViewController.h"
#import "NewAnimalViewController.h"
#import "ViewController.h"

NSString * const kCellIdentifier = @"CellId";
NSString * const kSearchResultCellIdentifier = @"kSearchResultCellIdentifier";

@interface ViewController ()<AnimalViewControllerDelegate,NewAnimalViewControllerDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *animals;
@property (nonatomic, strong) NSMutableArray *filteredAnimals;

@property (nonatomic, strong) UISearchDisplayController *searchController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    
    [self.view addSubview:self.tableView];
    
    self.animals = [@[@"Dog",@"Cat",@"Bird",@"Fish",@"Cow"] mutableCopy];
    
    [self showEditingButton];
    [self showAddButton];
    [self showSearchBar];
}

//Practica 6: Borrar Celda

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView)
    {
        return self.animals.count;
    }
    else
    {
        return self.filteredAnimals.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        return [self regularCellAtIndexPath:indexPath];
    }
    else
    {
        return [self searchResultCellAtIndexPath:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.animals removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (UITableViewCell *)regularCellAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    cell.textLabel.text = self.animals[indexPath.row];
    cell.showsReorderControl = YES;
    return cell;
}

//Practica 7: Borrar celdas desde el Detalle

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AnimalViewController *animalViewController = [[AnimalViewController alloc] init];
    animalViewController.animalName = self.animals[indexPath.row];
    animalViewController.delegate = self;
    [self.navigationController pushViewController:animalViewController animated:YES];
}

#pragma mark - AnimalViewControllerDelegate Methods

- (void)animalDeleted:(NSString *)animalName
{
    NSUInteger animalIndex = [self.animals indexOfObject:animalName];
    [self.animals removeObject:animalName];
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:animalIndex inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//Practica 8: Mover celdas

- (void)showEditingButton
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(startEditingMode)];
}

- (void)startEditingMode
{
    [self.tableView setEditing:YES animated:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(finishEditingMode)];
}

- (void)finishEditingMode
{
    [self.tableView setEditing:NO animated:YES];
    [self showEditingButton];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSString *animal = self.animals[sourceIndexPath.row];
    [self.animals removeObjectAtIndex:sourceIndexPath.row];
    [self.animals insertObject:animal atIndex:destinationIndexPath.row];
}

//Practica 9: Añadir celdas

- (void)showAddButton
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(showNewAnimalViewController)];
}

- (void)showNewAnimalViewController
{
    NewAnimalViewController *newAnimalViewController = [[NewAnimalViewController alloc] init];
    newAnimalViewController.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newAnimalViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - NewAnimalViewControllerDelegate Methods

- (void)newAnimal:(NSString *)animal
{
    [self.animals addObject:animal];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.animals.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//Practica 10. Búsqueda

- (void)showSearchBar
{
    self.filteredAnimals = [@[] mutableCopy];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
    searchBar.delegate = self;
    self.tableView.tableHeaderView = searchBar;
    self.searchController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar
                                                              contentsController:self];
    [self setSearchController:self.searchController];
    self.searchController.delegate = self;
    self.searchController.searchResultsDataSource = self;
    [self.searchController.searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kSearchResultCellIdentifier];
}

#pragma mark - UISearchDisplayDelegate Methods

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString];
    return YES;
}

-(void)filterContentForSearchText:(NSString*)searchText
{
    [self.filteredAnimals removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
    self.filteredAnimals = [NSMutableArray arrayWithArray:[self.animals filteredArrayUsingPredicate:predicate]];
}

- (UITableViewCell *)searchResultCellAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *searchResultsCell = [self.searchController.searchResultsTableView dequeueReusableCellWithIdentifier:kSearchResultCellIdentifier];
    searchResultsCell.textLabel.text = self.filteredAnimals[indexPath.row];
    return searchResultsCell;
}

@end

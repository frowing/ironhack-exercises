//
//  NewAnimalViewController.h
//  SecondTableView
//
//  Created by Francisco Sevillano on 07/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewAnimalViewControllerDelegate <NSObject>

- (void)newAnimal:(NSString *)animal;

@end

@interface NewAnimalViewController : UIViewController

@property (nonatomic, weak) id<NewAnimalViewControllerDelegate> delegate;

@end

//
//  AnimalViewController.m
//  SecondTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "AnimalViewController.h"

@implementation AnimalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deletePressed)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    label.text = self.animalName;
    [label sizeToFit];
    label.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:label];
}

- (void)deletePressed
{
    [self.delegate animalDeleted:self.animalName];
    [self.navigationController popViewControllerAnimated:YES];
}

@end

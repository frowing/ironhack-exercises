//
//  ViewController.m
//  FirstTableView
//
//  Created by Francisco Sevillano on 06/02/15.
//  Copyright (c) 2015 Ironhack. All rights reserved.
//

#import "EvenNumberCell.h"
#import "ViewController.h"
#import "OddNumberCell.h"

NSString * const kEvenCellIdentifier = @"EvenCell";
NSString * const kOddCellIdentifier = @"OddCell";

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.tableView registerClass:[EvenNumberCell class] forCellReuseIdentifier:kEvenCellIdentifier];
    [self.tableView registerClass:[OddNumberCell class] forCellReuseIdentifier:kOddCellIdentifier];
    
    [self.view addSubview:self.tableView];
}

//Practica 1: Crear y poblar una Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0)
    {
        return [self evenCellForIndexPath:indexPath];
    }
    else
    {
        return [self oddCellForIndexPath:indexPath];
    }
}

//Practica 2: Añadir secciones con header

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"First 5 Items";
    }
    else
    {
        return @"Second 5 Items";
    }
}

//Practica 3: Personalizar las celdas

- (EvenNumberCell *)evenCellForIndexPath:(NSIndexPath *)indexPath
{
    EvenNumberCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kEvenCellIdentifier];
    cell.textLabel.textColor = [UIColor redColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row];
    return cell;
}

- (OddNumberCell *)oddCellForIndexPath:(NSIndexPath *)indexPath
{
    OddNumberCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kOddCellIdentifier];
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row];
    return cell;
}

//Practica 4: Seleccionar la celda

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[[UIAlertView alloc] initWithTitle:@"Cell Selected"
                                message:[NSString stringWithFormat:@"Cell %ld was selected",indexPath.row]
                               delegate:nil
                      cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]
     show];
}

//Practica 5: Configura el header

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    
    if (section == 0)
    {
        label.text = @"First Header";
    }
    else
    {
        label.text = @"Second Header";
    }
    
    [label sizeToFit];
    
    label.center = CGPointMake(view.frame.size.width / 2, view.frame.size.height / 2);
    [view addSubview:label];
    return view;
}




@end
